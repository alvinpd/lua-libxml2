lua-libxml2: libxml2 binding for Lua
====================================

This is an implementation of a Lua binding for the libxml2 XML parser
and toolkit.

