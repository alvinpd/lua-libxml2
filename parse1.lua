-- 

require ("lxml2")

-- Parse the content of the storyinfo node
--

function parse_storyinfo(doc, cur)
	print("parsing node: " .. cur:Name())
	cur = cur:ChildrenNode()
	while cur do
		if cur:Name() == "author" then
			print("Author: " .. doc:NodeListGetString(cur))
		elseif cur:Name() == "datewritten" then
			print("Date: " .. doc:NodeListGetString(cur))
		elseif cur:Name() == "keyword" then
			print("Keyword: " .. doc:NodeListGetString(cur))
		end

		cur = cur:Next()
	end

	print()
end

function parse_body(doc, cur)
	print("parsing node: " .. cur:Name())
	cur = cur:ChildrenNode()
	while cur do
		if cur:Name() == "headline" then
			print("Headline: " .. doc:NodeListGetString(cur))
		elseif cur:Name() == "para" then
			print("Paragraph: " .. doc:NodeListGetString(cur))
		end

		cur = cur:Next()
	end

	print()
end

--

doc = lxml2.ReadFile("test.xml", nil, 0)

if doc == nil then
	print("failed to read xml file")
end

cur = doc:GetRootElement()

if cur then
	cur = cur:ChildrenNode()

	while cur do
		if cur:Name() == "storyinfo" then
			parse_storyinfo(doc, cur)
		elseif cur:Name() == "body" then
			parse_body(doc, cur)
		end

		cur = cur:Next()
	end
end

print("parse1.lua completed")

-- End of file
