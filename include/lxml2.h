/*
 * File:   lxml2.h
 * Author: difuntoruma
 *
 * Created on November 19, 2012, 3:18 PM
 */

#ifndef __LUA_LIBXML2_H__
#define	__LUA_LIBXML2_H__

#define lua_c

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#define lxml2_dbg(fmt,...)	fprintf(stdout, "%s %d: " fmt, \
					__func__, __LINE__, ##__VA_ARGS__)
#define lxml2_info(fmt,...)	fprintf(stdout, fmt, ##__VA_ARGS__)

struct lxml2Object {
	xmlDocPtr doc;
};


int luaopen_lxml2(lua_State *L);

#endif	/* __LUA_LIBXML2_H__ */
