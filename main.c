/*
 * File:   main.c
 * Author: difuntoruma
 *
 * Created on November 19, 2012, 3:12 PM
 */

#include <stdio.h>
#include <stdlib.h>

#include "lxml2.h"

/*
 *
 */
int main(int argc, char** argv)
{
	int stat;
	lua_State *L;

	L = luaL_newstate();
	if (L == NULL) {
		fprintf(stderr, "cannot create state: not enough memory\n");
		return EXIT_FAILURE;
	}

	/* Initialize libxml2 library */
	xmlInitParser();
        LIBXML_TEST_VERSION

	luaL_checkversion(L);
	lua_gc(L, LUA_GCSTOP, 0); /* Stop collector during initialization */
	luaL_openlibs(L);         /* Open libraries*/
	lua_gc(L, LUA_GCRESTART, 0);

	luaL_requiref(L, "lxml2", luaopen_lxml2, 1);
	lua_pop(L, 1);

	stat = luaL_loadfile(L, "parse1.lua");
	if (stat == LUA_OK) {
		stat = lua_pcall(L, 0, 0, 0);
		if (stat != LUA_OK) {
			const char *msg = (lua_type(L, -1) == LUA_TSTRING) ?
				lua_tostring(L, -1) : NULL;
			fprintf(stderr, "%s\n", msg);
		}
	}

	/* Finalize libxml2 library*/
        xmlCleanupParser();
	xmlMemoryDump();    /* This is to debug memory for regression tests */

	lua_close(L);

	printf("-- Run done --\n");

	return EXIT_SUCCESS;
}

/* End of file */
